var app = require('http').createServer(handler)
var io = require('socket.io')(app);
var fs = require('fs');



app.listen(2340);
console.log("server is running...")
// server.listen(3000);

function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

io.on('connection', function (socket) {
  console.log("a user connected")
  socket.broadcast.emit('message s', 'A client Connet');
  // socket.emit('message', 'You are connected!');


  socket.on('sendmessage', function(data){
    console.log(data);
    socket.broadcast.emit('newmessage', {msg : data});
  });

  socket.on('sendpostion', function(data){
    console.log(data);
    socket.broadcast.emit('getpostion', {msg : data});
    // io.socket.emit('getpostion',{post: data});
  });


  // Disconnect
  socket.on('disconnection', function (data) {
    socket.disconnect();
    connections.splice(connections.indexOf(socket), 1);
    console.log("User disconnected");
  });

  // broadcast
  //emit to everyone
  // socket.broadcast.emit('message', 'Message to all units. I repeat, message to all units.');


});
